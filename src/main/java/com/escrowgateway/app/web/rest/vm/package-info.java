/**
 * View Models used by Spring MVC REST controllers.
 */
package com.escrowgateway.app.web.rest.vm;
